import sys
import time
from PyQt5.QtCore import (Qt, QTimer)
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QInputDialog, QVBoxLayout, QDesktopWidget
from PyQt5.QtGui import QPainter, QPen, QColor, QPainterPath, QCursor, QFont
import math
import keyboard
from copy import copy

app = QApplication(sys.argv)
message = 'hello'


class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def to_xy(self):
        return self.x, self.y


class Window(QWidget):
    def __init__(self, angle=81, my_position=(534, 808), power=100, distance=0, parent=None):
        super().__init__(parent)
        self.setWindowTitle('test')
        # self.setWindowFlag(Qt.CustomizeWindowHint)
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setWindowFlags(Qt.Widget | Qt.FramelessWindowHint)
        self.setStyleSheet("background:transparent")
        self.angle = angle
        self.my_position = Point(*my_position)
        self.distance = distance
        self.velocity = power
        self.wind = 0
        self.desktop = QDesktopWidget()
        # self.ratio = self.screen().devicePixelRatio()
        self.size = self.desktop.availableGeometry(
            self.desktop.primaryScreen())
        # self.size.setWidth(self.size.width())
        # self.size.setHeight(self.size.height())
        # self.radius = self.size.width() * 286 / 1792
        self.move(0, 0)
        self.setBaseSize(self.size.width(), self.size.height())
        self.resize(self.size.width(), self.size.height())
        print(self.size.width(), self.size.height())
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.CustomizeWindowHint)
        # self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        # self.setWindowState(Qt.WindowFullScreen)
        # self.setWindowState(Qt.WindowFullScreen)
        keyboard.add_hotkey('ctrl+alt+q', lambda: self.hide_or_show())
        keyboard.add_hotkey('shift+o', lambda: self.hide_or_show())

    def update(self):
        self.repaint()

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        if event.key() == Qt.Key_Escape:
            self.close()
        if event.key() == Qt.Key_S:
            self.my_position = Point(QCursor.pos().x(), QCursor.pos().y())
            self.update()
        # if event.key() == Qt.Key_R:
        #     radius_point = Point(QCursor.pos().x(), QCursor.pos().y())
        #     self.radius = math.sqrt(
        #         (radius_point.x-self.my_position.x)**2 + (radius_point.y-self.my_position.y)**2)
        #     self.update()
        if event.key() == Qt.Key_A:
            num, ok = QInputDialog.getInt(
                self, "Angle", "enter the angle", self.angle)
            if ok:
                self.angle = num
                self.update()
        if event.key() == Qt.Key_P:
            num, ok = QInputDialog.getInt(
                self, "Power", "enter the power", self.velocity)
            if ok:
                self.velocity = num
                self.update()
        if event.key() == Qt.Key_W:
            num, ok = QInputDialog.getInt(
                self, "Wind", "enter the wind", self.wind)
            if ok:
                self.wind = num
                self.update()
        if event.key() == Qt.Key_Up:
            if self.velocity < 100:
                self.velocity += 1
                self.update()
        if event.key() == Qt.Key_Down:
            if self.velocity > 0:
                self.velocity -= 1
                self.update()
        if event.key() == Qt.Key_Left:
            self.angle += 1
            self.update()
        if event.key() == Qt.Key_Right:
            self.angle -= 1
            self.update()

    def hide_or_show(self):
        print(12)
        if not self.isHidden():
            self.hide()
            return
        self.show()

    def paintEvent(self, a0: QtGui.QPaintEvent):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        qp.setBrush(Qt.blue)
        qp.drawEllipse(self.my_position.x-10, self.my_position.y-10, 20, 20)
        qp.setBrush(Qt.transparent)
        qp.setPen(Qt.red)
        qp.setPen(Qt.green)
        qp.setFont(QFont('Decorative', 10))
        a = self.angle - int(self.angle / 90) * 90
        angle = 90 - a if self.angle > 90 else a
        qp.drawText(100, 200, f'power = {self.velocity}')
        qp.drawText(100, 220, f'angle = {angle}')
        # qp.drawText(100, 240, f'power = {self.radius}')
        qp.drawText(100, 260, f'wind = {self.wind}')
        qp.drawText(100, 270, f'my_position = {self.my_position.to_xy()}')
        qp.drawText(self.my_position.x, self.my_position.y + 30,
                    f'my_position = {self.my_position.to_xy()}')
        # qp.end()
        qp.setPen(Qt.green)
        current_coord = copy(self.my_position)
        width = self.size.width()
        height = self.size.height()
        velocity = self.velocity / 100
        velocity_x = velocity * math.cos(math.radians(self.angle))
        velocity_y = velocity * math.sin(math.radians(self.angle))
        wind_acceleration = 45/1561784/53 * self.wind
        # r_scale = self.radius / 286
        w_scale = self.width() / 1792
        acceleration_y = -0.000427715996578
        time = 0
        time_increment = 1
        # print(current_coord.y, height)
        C = math.sin(math.radians(self.angle)) / \
            (math.cos(math.radians(self.angle)))
        while (current_coord.x < width and current_coord.y < height):
            time += time_increment
            current_coord.x = (velocity_x*time + 0.5 * (wind_acceleration)*time**2) * \
                w_scale + self.my_position.x
            current_coord.y = (-(velocity_y*time + 0.5 * acceleration_y *
                                 time**2)) * w_scale + self.my_position.y
            # print(current_coord.to_xy())

            # current_coord.x += 0.1
            # current_coord.y = -(C * current_coord.x + 0.5 *
            #                     acceleration_y * (current_coord.x / velocity_x)**2) + self.my_position.y
            qp.drawPoint(*current_coord.to_xy())

        qp.end()


def run():
    w = Window()
    w.show()
    app.exec()


if __name__ == '__main__':
    run()
