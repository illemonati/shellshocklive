from pykeyboard import PyKeyboard
from pymouse import PyMouse
import time

k = PyKeyboard()
m = PyMouse()


def turn_r_to_0():
    for i in range(100):
        k.tap_key(k.down_key)
        time.sleep(0.06)


def shoot():
    m.click(1229,981)


if __name__ == '__main__':
    time.sleep(3)
    print(m.position())
    turn_r_to_0()
    while True:
        time.sleep(2)
        shoot()
