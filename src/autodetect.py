import cv2
import pyautogui
import numpy as np
import math
import keyboard

#################################
# Settings
RADIUS = 330  # measured from the center of tank to the border of the aiming circle
WIND = 0  # wind of the game, if no wind, this is 0, if wind toward enemy put positive wine, if wind toward self, put negative value
BOTTOM_LINE = 922  # The Line On the bottom where the controls start
TANK_MIN_SIZE = 300  # MIN and MAX size the rectangles around the tanks are, you can probebly figure out the exact one but gussing around with this is easier
TANK_MAX_SIZE = 850
STARTING_ANGLE = 90  # This changes each time, so please change it accordingly, or you can change the ingame angle to the one you set each game
STARTING_POWER = 99
################################


class AutoDetectAimBot:
    def __init__(self):
        self.screenshot = None
        self.screenshot_hsv = None
        self.red_centers = []
        self.green_centers = []
        self.angle = STARTING_ANGLE
        self.power = STARTING_POWER

    def run(self):
        self.get_screenshot()
        self.get_red()
        self.get_green()
        for coord in self.green_centers:
            for rcoord in self.red_centers:
                if math.sqrt((coord[0] - rcoord[0]) ** 2) < 5:
                    self.red_centers.remove(rcoord)
            print(f"self: {coord}")
        for number, coord in enumerate(self.red_centers):
            print(f"enemy: {coord} {number+1}")
            keyboard.add_hotkey(f"alt+{number+1}", self.target, [(number+1)])
        keyboard.add_hotkey("alt+r", self.rerun)
        while True:
            time.sleep(1)

    def rerun(self):
        self.red_centers = []
        self.green_centers = []
        self.run()

    def get_screenshot(self):
        self.screenshot = np.array(pyautogui.screenshot())
        self.screenshot_hsv = cv2.cvtColor(self.screenshot, cv2.COLOR_RGB2HSV)

    def get_red(self):
        screenshot = self.screenshot
        lower_red = np.array([0, 120, 70])
        upper_red = np.array([10, 255, 255])
        mask1 = cv2.inRange(self.screenshot_hsv, lower_red, upper_red)
        lower_red = np.array([170, 120, 70])
        upper_red = np.array([180, 255, 255])
        mask2 = cv2.inRange(self.screenshot_hsv, lower_red, upper_red)
        mask_red = mask1 + mask2
        res = cv2.bitwise_and(screenshot, screenshot, mask=mask_red)
        (contours, hierarchy) = cv2.findContours(mask_red, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for pic, contour in enumerate(contours):
            area = cv2.contourArea(contour)
            if TANK_MIN_SIZE < area < TANK_MAX_SIZE:
                x, y, w, h = cv2.boundingRect(contour)
                if y < BOTTOM_LINE:
                    self.red_centers.append((x + w / 2, y + h / 2))

    def get_green(self):
        screenshot = self.screenshot
        lower_green = np.array([65, 60, 60])
        upper_green = np.array([80, 255, 255])
        mask_green = cv2.inRange(self.screenshot_hsv, lower_green, upper_green)
        res = cv2.bitwise_and(screenshot, screenshot, mask=mask_green)
        (contours, hierarchy) = cv2.findContours(mask_green, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for pic, contour in enumerate(contours):
            area = cv2.contourArea(contour)
            if TANK_MIN_SIZE < area < TANK_MAX_SIZE:
                x, y, w, h = cv2.boundingRect(contour)
                if y < BOTTOM_LINE:
                    self.green_centers.append((x + w / 2, y + h / 2))

    def calculate_angle(self, tank_number):
        distance = -(self.green_centers[0][0] - self.red_centers[tank_number-1][0])
        angle = (90 - 4 * (distance / RADIUS) + WIND / 14)
        return angle

    def target(self, number):
        print("targeting {}".format(self.red_centers[number-1]))
        angle = round(self.calculate_angle(number))
        print(angle)
        adjustment_angle = angle - self.angle
        self.change_angle(adjustment_angle)
        self.angle = self.angle + adjustment_angle

    @staticmethod
    def change_angle(adjustment_angle):
        print(adjustment_angle)
        if adjustment_angle < 0:
            for angle in range(-adjustment_angle):
                keyboard.send("right arrow")
                time.sleep(0.1)
        else:
            for angle in range(adjustment_angle):
                # print(1)
                keyboard.send("left arrow")
                time.sleep(0.1)


if __name__ == '__main__':
    import time
    time.sleep(1)
    adab = AutoDetectAimBot()
    adab.run()

