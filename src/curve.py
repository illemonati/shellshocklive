import sys
import time
from PyQt5.QtCore import (Qt, QTimer)
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QInputDialog, QVBoxLayout, QDesktopWidget
from PyQt5.QtGui import QPainter, QPen, QColor, QPainterPath, QCursor, QFont
import math
import keyboard

app = QApplication(sys.argv)
message = 'hello'


class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def to_xy(self):
        return self.x, self.y


class Window(QWidget):
    def __init__(self, angle=81, my_position=(534, 808), power=100, distance=0, parent=None):
        super().__init__(parent)
        self.setWindowTitle('test')
        self.setWindowFlag(Qt.CustomizeWindowHint)
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setWindowFlags(Qt.Widget | Qt.FramelessWindowHint)
        self.angle = angle
        self.my_position = Point(*my_position)
        self.distance = distance
        self.velocity = power
        self.desktop = QDesktopWidget()
        self.size = self.desktop.availableGeometry(
            self.desktop.primaryScreen())
        self.setBaseSize(self.size.width(), self.size.height())
        # self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.CustomizeWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowState(Qt.WindowFullScreen)
        keyboard.add_hotkey('ctrl+alt+q', lambda: self.hide_or_show())

    def update(self):
        self.repaint()

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        if event.key() == Qt.Key_Escape:
            self.close()
        if event.key() == Qt.Key_S:
            self.my_position = Point(QCursor.pos().x(), QCursor.pos().y())
            self.update()
        if event.key() == Qt.Key_A:
            num, ok = QInputDialog.getInt(
                self, "Angle", "enter the angle", self.angle)
            if ok:
                self.angle = num
                self.update()
        if event.key() == Qt.Key_P:
            num, ok = QInputDialog.getInt(
                self, "Power", "enter the power", self.velocity)
            if ok:
                self.velocity = num
                self.update()
        if event.key() == Qt.Key_Up:
            if self.velocity < 100:
                self.velocity += 1
                self.update()
        if event.key() == Qt.Key_Down:
            if self.velocity > 0:
                self.velocity -= 1
                self.update()
        if event.key() == Qt.Key_Left:
            self.angle += 1
            self.update()
        if event.key() == Qt.Key_Right:
            self.angle -= 1
            self.update()

    def hide_or_show(self):
        print(12)
        if not self.isHidden():
            self.hide()
            return
        self.show()

    def calculate(self):
        radians = math.radians(self.angle)
        self.velocity = self.velocity
        max_height_point = Point()
        h = (self.velocity * self.velocity * math.sin(radians) *
             math.sin(radians) / 20 * 1.67 * (self.width() / 1280))
        # max_height_point.y = self.height() - h
        try:
            a = 4 * h / math.tan(radians)
        except ZeroDivisionError:
            a = 0
        # a= 798
        # a = self.distance
        print(self.distance)
        print(a)
        try:
            c = -math.tan(radians) / a * 1.02
        except ZeroDivisionError:
            c = 0
        max_height_point.x = self.my_position.x + a/2
        print((self.my_position.x, max_height_point.x,
               max_height_point.x - self.my_position.x))
        x = 0
        # while not (x > self.width()):
        #     max_height_point.y = c * ((x - self.my_position.x) - a) * (x - self.my_position.x)
        #     x += 1
        x = math.ceil(self.width())
        max_height_point.y = c * \
            ((x - self.my_position.x) - a) * (x - self.my_position.x)
        return self.my_position.x, self.my_position.y, max_height_point.x, max_height_point.y, self.my_position.x + a, self.my_position.y

    def paintEvent(self, a0: QtGui.QPaintEvent):
        qp = QPainter()
        p = QPainterPath()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        qp.setBrush(Qt.blue)
        qp.drawEllipse(self.my_position.x-10, self.my_position.y-10, 20, 20)
        qp.setBrush(Qt.transparent)
        qp.setPen(Qt.red)
        # qp.setBrush(Qt.red)
        p.moveTo(*self.my_position.to_xy())
        # print(self.calculate())
        p.cubicTo(*self.calculate())
        # qp.drawArc(100, 100, 0, , 0 * 16, 90 * 16)
        qp.drawPath(p)
        qp.setPen(Qt.green)
        qp.setFont(QFont('Decorative', 10))
        a = self.angle - int(self.angle / 90) * 90
        angle = 90 - a if self.angle > 90 else a
        qp.drawText(100, 220, f'power = {self.velocity}')
        qp.drawText(100, 200, f'angle = {angle}')
        qp.drawText(100, 240, f'my_position = {self.my_position.to_xy()}')
        qp.drawText(self.my_position.x, self.my_position.y + 30,
                    f'my_position = {self.my_position.to_xy()}')
        qp.end()


def run():
    w = Window()
    w.show()
    app.exec()


if __name__ == '__main__':
    run()
