from pymouse import PyMouse, PyMouseEvent
import pymouse
from pykeyboard import PyKeyboard, PyKeyboardEvent
import math
import plyer
from threading import Thread
import threading
import time
import subprocess

setting = ''
s = None
o = None
r = None
w = 0
p = 100
radius = 0


def calc_angle(power, distance, radius, wind):
    try:
        return (power/100) * (90-4 * (distance/radius)+wind/14)
    except Exception as e:
        print(f'Error : {e}')


def calc():
    global setting, s, o, r, w, p, radius
    # print(f"{s}, {o}, {r}, {w}")
    # distance = math.sqrt(((s[0] - o[0]) ** 2) + ((s[1] - o[1]) ** 2))
    distance = math.sqrt((s[0] - o[0])**2)
    angle = calc_angle(p, distance, radius, w)
    print(f"distance = {distance}")
    print(f"radius = {radius}")
    print(f"angle = {angle}")
    plyer.notification.notify('angle', f"angle = {calc_angle(p, distance, radius, w)}")


class DetectStart(PyKeyboardEvent):
    def __init__(self):
        PyKeyboardEvent.__init__(self)
        self.mouse = PyMouse()
        self.on_wind_input = False
        self.on_power_input = False
        self.wind = ""
        self.power = ""

    def tap(self, keycode, character, press):
        global setting, r, o, s, w, p, radius
        setting = character
        if press:
            if self.on_wind_input:
                if character == "`":
                    self.on_wind_input = False
                    w = int(self.wind)
                    print(f"w = {w}")
                    self.wind = ""
                    return
                else:
                    self.wind += character
                return
            elif self.on_power_input:
                if character == "`":
                    self.on_power_input = False
                    w = int(self.power)
                    print(f"p = {w}")
                    self.power = ""
                    return
                else:
                    self.power += character
                return
            elif character == 'c':
                calc()
                return
            elif character == 'r':
                r = self.mouse.position()
                radius = math.sqrt(((s[0] - r[0]) ** 2) + ((s[1] - r[1]) ** 2))
            elif character == 's':
                s = self.mouse.position()
            elif character == 'o':
                o = self.mouse.position()
            elif character == 'w':
                self.on_wind_input = True
                return
            elif character == 'p':
                self.on_power_input = True
                return
            else:
                return
            print(f"{setting} = {self.mouse.position()}")


if __name__ == '__main__':
    while True:
        try:
            ds = DetectStart()
            ds.run()
        except Exception as e:
            pass
